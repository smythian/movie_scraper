import json
import re
import unittest
from os import path, remove
from shutil import rmtree
from tempfile import mkdtemp

import common
from box_office_mojo import BoxOfficeMojo
from imdb import IMDB
from rotten_tomatoes import RottenTomatoes

TEST_DATA_PATH = path.join(path.dirname(__file__), 'test_data')
BOM_PATH = path.join(TEST_DATA_PATH, 'box_office_mojo')
BOM_SEARCH_PATH = path.join(BOM_PATH, 'search')
BOM_BOX_OFFICE_PATH = path.join(BOM_PATH, '{0}_box_office')
RT_PATH = path.join(TEST_DATA_PATH, 'rotten_tomatoes')
RT_SEARCH_PATH = path.join(RT_PATH, 'search')
RT_DETAIL_PATH = path.join(RT_PATH, 'detail')
RT_REVIEWS_PATH = path.join(RT_PATH, 'reviews')
common.MAX_REQUESTS_PER_SEC = 0

file_handles = list()


def setup():
    global file_handles
    file_handles = list()


def teardown():
    for fh in file_handles:
        if not fh.closed:
            fh.close()


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, filepath=None, is_binary=False, status_code=200):
            if filepath is not None:
                if is_binary:
                    fh = open(filepath, 'rb')
                    file_handles.append(fh)
                    self.__file__ = fh
                else:
                    with open(filepath, 'r') as f:
                        self.text = f.read()
            self.url = args[0]
            self.status_code = status_code

        def iter_content(self, chunk_size=1024):
            return iter(lambda: self.__file__.read(chunk_size), b"")

        def raise_for_status(self):
            pass

        def json(self):
            return json.loads(self.text)

    rt_detail_match = re.match('^{0}/m/([\w\d]+)$'.format(RottenTomatoes.URL), args[0])
    rt_id_match = re.match('^{0}(\d+)$'.format(RottenTomatoes.MOVIE_API_URL), args[0])
    bom_movies_match = re.match('^{0}/movies/\?id=([\w\d]+).htm&page=(weekly|weekend|daily)'
                                .format(BoxOfficeMojo.BOM_URL), args[0])
    title = kwargs['params']['q'] if 'params' in kwargs and 'q' in kwargs['params'] else ''

    # IMDB queries
    if args[0] == '{0}{1}.gz'.format(IMDB.DATASET_URL, IMDB.BASICS_FILE):
        result = MockResponse(path.join(TEST_DATA_PATH, '{0}.gz'.format(IMDB.BASICS_FILE)), True)
    elif args[0] == '{0}{1}.gz'.format(IMDB.DATASET_URL, IMDB.RATINGS_FILE):
        result = MockResponse(path.join(TEST_DATA_PATH, '{0}.gz'.format(IMDB.RATINGS_FILE)), True)
    # Box Office Mojo queries
    elif args[0] == BoxOfficeMojo.QUERY_URL:
        result = MockResponse(path.join(BOM_SEARCH_PATH, '{0}.html'.format(title.replace(':', '-'))))
    elif bom_movies_match:
        dir_path = BOM_BOX_OFFICE_PATH.format(bom_movies_match.group(2))
        result = MockResponse(path.join(dir_path, '{0}.html'.format(bom_movies_match.group(1))))
    # Rotten Tomato queries
    elif args[0] == RottenTomatoes.SEARCH_API_URL:
        result = MockResponse(path.join(RT_SEARCH_PATH, '{0}.json'.format(title.replace(':', '-'))))
    elif rt_detail_match:
        result = MockResponse(path.join(RT_DETAIL_PATH, '{0}.html'.format(rt_detail_match.group(1))))
    elif rt_id_match:
        result = MockResponse(path.join(RT_REVIEWS_PATH, '{0}.json'.format(rt_id_match.group(1))))
    else:
        result = MockResponse(status_code=404)

    return result


class TestCommon(unittest.TestCase):
    def test_exactish_match(self):
        self.assertTrue(common.exactish_match('Kate & Leopold', 'Kate and Leopold'))
        self.assertTrue(common.exactish_match('Men in Black II', 'Men in Black 2'))

    def test_flatten(self):
        original = {
            'foo': [1, 2, 3],
            'bar': {
                'do': 're',
                'mi': 'fa'
            }
        }
        expected = {
            'foo': [1, 2, 3],
            'bar_do': 're',
            'bar_mi': 'fa'
        }
        self.assertEqual(common.flatten_obj(original), expected)

    def test_save_and_load(self):
        abs_path = mkdtemp()
        original_json = [
            {
                'foo': [1, 2, 3],
                'bar': {
                    'do': 're',
                    'mi': 'fa'
                }
            }, {
                'foo': [4, 5],
                'bar': {
                    'mi': 'fa',
                },
                'uh': 'oh'
            }
        ]
        original_flattened = [
            {'bar_do': 're', 'bar_mi': 'fa', 'foo': '[1, 2, 3]', 'uh': ''},
            {'bar_do': '', 'bar_mi': 'fa', 'foo': '[4, 5]', 'uh': 'oh'}
        ]

        try:
            json_file = path.join(abs_path, 'test.json')
            self.assertFalse(path.exists(json_file))
            common.save_results(original_json, json_file)
            self.assertTrue(path.exists(json_file))
            self.assertEqual(original_json, common.load_results(json_file))
            remove(json_file)
            self.assertFalse(path.exists(json_file))

            for csv_file in [path.join(abs_path, 'test.csv'), path.join(abs_path, 'test.tsv')]:
                self.assertFalse(path.exists(csv_file))
                common.save_results(original_json, csv_file)
                self.assertTrue(path.exists(csv_file))
                self.assertEqual(original_flattened, common.load_results(csv_file))
                remove(csv_file)
                self.assertFalse(path.exists(csv_file))

            with self.assertRaises(RuntimeError):
                common.save_results(original_json, path.join(abs_path, 'foo.bar'))
            with self.assertRaises(RuntimeError):
                common.load_results(path.join(abs_path, 'foo.bar'))
        finally:
            rmtree(abs_path)
