#!/usr/bin/env python

import argparse
from datetime import datetime
from os import makedirs, path

from box_office_mojo import BoxOfficeMojo
from common import save_results, init_file_logger, logger, load_results
from imdb import IMDB
from rotten_tomatoes import RottenTomatoes

__version__ = '0.1.0'

MODULE_PATH = path.dirname(path.abspath(__file__))
INTERIM_FILE_PATH = path.join(MODULE_PATH, 'interim_results')
MIN_YEAR = datetime.now().year - 1
MAX_YEAR = datetime.now().year
MIN_VOTES = 160
BOX_OFFICE = 'weekend'


def parse_args():
    parser = argparse.ArgumentParser(description='''
    Pull movie data from IMDB, Box Office Mojo, and Rotten Tomatoes. This script will automatically resume execution 
    from the last run. To start over, ensure the results path does not contain files from a previous run.
    ''')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s version ' + __version__)
    parser.add_argument(
        '-o',
        '--oldest',
        default=MIN_YEAR,
        type=int,
        help='Earliest release year to include. Default is "{0}".'.format(MIN_YEAR),
    )
    parser.add_argument(
        '-n',
        '--newest',
        default=MAX_YEAR,
        type=int,
        help='Last release year to include. Default is "{0}".'.format(MAX_YEAR),
    )
    parser.add_argument(
        '-r',
        '--ratings',
        default=MIN_VOTES,
        type=int,
        help='Minimum number of ratings on IMDB (99.5 percent have fewer than 160). Default is "{0}".'
        .format(MIN_VOTES),
    )
    parser.add_argument(
        '-b',
        '--box',
        default=BOX_OFFICE,
        help='Box office data to pull. Options are "daily", "weekend", or "weekly". Default is "{0}".'
        .format(BOX_OFFICE),
    )
    parser.add_argument(
        '-f',
        '--format',
        default='json',
        help='File format for the results. Options are "csv", "tsv", and "json" (default).',
    )
    parser.add_argument(
        '-p',
        '--path',
        default=INTERIM_FILE_PATH,
        help='Directory where results should be saved. Default is "{0}".'.format(INTERIM_FILE_PATH),
    )
    return parser.parse_args()


def main():
    args = parse_args()
    if not path.exists(args.path):
        makedirs(args.path)
    init_file_logger(args.path)
    imdb = IMDB(args.path)
    bom = BoxOfficeMojo(args.path, imdb.results_path)
    rt = RottenTomatoes(args.path, bom.results_path)

    imdb.run(args.oldest, args.newest, args.ratings)
    bom.run(args.box)
    rt.run()

    results_path = path.join(args.path, 'results.{0}'.format(args.format))
    logger.info('Saving results to "{0}"'.format(results_path))
    save_results(load_results(rt.results_path), results_path)
    logger.info('Results saved to "{0}"'.format(results_path))


if __name__ == '__main__':
    main()
