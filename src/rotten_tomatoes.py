import json
from os import path
from requests import exceptions

from bs4 import BeautifulSoup

from common import get_request, exactish_match, logger


class RottenTomatoes:
    URL = 'https://www.rottentomatoes.com'
    SEARCH_API_URL = '{0}/api/private/v2.0/search/'.format(URL)
    MOVIE_API_URL = '{0}/api/private/v1.0/movies/'.format(URL)
    FILENAME = 'rotten_tomatoes.results'

    @staticmethod
    def get_err_handler(err):
        result = True
        if isinstance(err, exceptions.HTTPError):
            if err.response.status_code == 404:  # Some movies with valid search results return 404
                result = False
        else:
            raise err
        return result

    @staticmethod
    def get_movie_id(title_url):
        result = None
        response = get_request(title_url, RottenTomatoes.get_err_handler)
        if response:
            movie_id_el = BeautifulSoup(response.text, "html5lib").select_one('meta[name="movieID"]')
            if movie_id_el:
                result = movie_id_el.attrs['content']
        return result

    @staticmethod
    def get_title_url(title, year):
        data = {'q': title}
        result = None
        response = get_request(RottenTomatoes.SEARCH_API_URL, RottenTomatoes.get_err_handler, params=data)
        if response:
            movies = filter(lambda movie: movie['year'] == year, response.json()['movies'])

            if len(movies) > 1:
                movies = filter(lambda movie: exactish_match(movie['name'], title), movies)

            if len(movies) == 1:
                result = u'{0}{1}'.format(RottenTomatoes.URL, movies[0]['url'])
            elif len(movies) == 0:
                logger.debug(u'No search results for "{0}" ({1})'.format(title, year))
            else:
                possibilities = u'"{0}"'.format(u'", "'.join(map(lambda movie: movie['name'], movies)))
                logger.warn(u'Search for "{0}" ({1}) could not be identified among: {2}'
                            .format(title, year, possibilities))

        return result

    def __init__(self, dir_path, titles_path):
        self.titles_path = titles_path
        self.results_path = path.join(dir_path, RottenTomatoes.FILENAME)

    def run(self):
        logger.debug('Beginning to scrape box office data')
        scrape_count = 0
        with open(self.results_path, 'a+') as results_file, open(self.titles_path, 'r') as titles_file:
            resume_count = 0
            success_count = 0
            total_count = 0
            for _ in results_file:
                resume_count += 1
                titles_file.next()
            logger.info('Loaded {0} movies from previous run'.format(resume_count))
            for row in titles_file:
                movie = json.loads(row.encode('utf-8'))
                url = RottenTomatoes.get_title_url(movie['title'], movie['year'])
                movie['rotten_tomatoes'] = {}
                if url:
                    movie_id = RottenTomatoes.get_movie_id(url)
                    if movie_id:
                        detail_url = '{0}{1}'.format(RottenTomatoes.MOVIE_API_URL, movie_id)
                        response = get_request(detail_url, RottenTomatoes.get_err_handler)
                        movie['rotten_tomatoes'] = response.json()
                results_file.write(u'{0}\n'.format(json.dumps(movie)).encode('utf-8'))
                scrape_count += 1
        logger.info('Completed scraping for {0} of {1} movies'.format(success_count, total_count))
