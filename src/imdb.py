from csv import DictReader
from os import path

import common


class IMDB:
    BASICS_FILE = 'title.basics.tsv'
    RATINGS_FILE = 'title.ratings.tsv'
    DATASET_URL = 'https://datasets.imdbws.com/'
    TITLE_TYPES = ['movie']
    FILENAME = 'imdb.results'

    @staticmethod
    def add_year(movie):
        sy = 0
        ey = 0
        try:
            sy = int(movie['startYear']) if 'startYear' in movie else 0
        except ValueError:
            pass
        try:
            ey = int(movie['endYear']) if 'endYear' in movie else 0
        except ValueError:
            pass
        movie['year'] = sy if sy > ey else ey
        return movie

    def __init__(self, dir_path):
        self.dir_path = dir_path
        self.results_path = path.join(dir_path, IMDB.FILENAME)
        self.basics_path = path.join(dir_path, IMDB.BASICS_FILE)
        self.ratings_path = path.join(dir_path, IMDB.RATINGS_FILE)
        self.ratings = dict()

    def add_ratings(self, movie):
        movie_id = movie['tconst']
        if movie_id in self.ratings:
            movie.update(self.ratings[movie_id])
        return {'imdb': movie, 'title': movie['primaryTitle'], 'year': movie['year']}

    def download_and_extract_file(self, filename):
        file_path = path.join(self.dir_path, filename)
        archive_name = '{0}.gz'.format(filename)
        archive_path = path.join(self.dir_path, archive_name)
        file_url = '{0}{1}'.format(IMDB.DATASET_URL, archive_name)
        if not path.exists(archive_path):
            common.logger.debug(u'Downloading "{0}"'.format(file_url))
            common.download_binary_file_in_chunks(file_url, archive_path)
        if not path.exists(file_path):
            common.logger.debug(u'Extracting to "{0}"'.format(file_path))
            common.extract_gzip_file(archive_path, file_path)

    def filter_and_merge_imdb_data(self, min_year, max_year, min_ratings):
        common.logger.debug('Merging ratings and title data')
        with open(self.basics_path, 'r') as basics_file:
            reader = DictReader(basics_file, delimiter='\t')
            movies = filter(lambda movie: movie['titleType'] in IMDB.TITLE_TYPES, reader)
            movies = map(IMDB.add_year, movies)
            movies = filter(lambda movie: min_year <= movie['year'] <= max_year, movies)
            movies = map(self.add_ratings, movies)
            result = filter(lambda movie: 'numVotes' in movie['imdb']
                                          and int(movie['imdb']['numVotes']) >= min_ratings, movies)
            result.sort(key=lambda movie: (movie['year'], movie['title']))
        common.logger.debug('Merged ratings and title data for {0} movies. Omitted {1} movies.'
                            .format(len(result), len(movies)))
        return result

    def load_ratings(self):
        common.logger.debug('Preparing ratings')
        with open(self.ratings_path, 'r') as ratings_file:
            reader = DictReader(ratings_file, delimiter='\t')
            for row in reader:
                self.ratings[row['tconst']] = {
                    'averageRating': row['averageRating'],
                    'numVotes': row['numVotes']
                }

    def run(self, min_year, max_year, min_ratings):
        self.download_and_extract_file(IMDB.BASICS_FILE)
        self.download_and_extract_file(IMDB.RATINGS_FILE)
        self.load_ratings()
        results = self.filter_and_merge_imdb_data(min_year, max_year, min_ratings)
        common.save_results(results, self.results_path)
        return results
