import unittest
from os import path
from shutil import rmtree, copyfile
from tempfile import mkdtemp

from mock import patch

import test_common
from box_office_mojo import BoxOfficeMojo
from common import load_results, save_results
from imdb import IMDB

TEST_DATA_PATH = path.join(path.dirname(__file__), 'test_data')
BOM_PATH = path.join(TEST_DATA_PATH, 'box_office_mojo')
BOM_SEARCH_PATH = path.join(BOM_PATH, 'search')
BOM_BOX_OFFICE_PATH = path.join(BOM_PATH, '{0}_box_office')
RT_PATH = path.join(TEST_DATA_PATH, 'rotten_tomatoes')
RT_SEARCH_PATH = path.join(RT_PATH, 'search')
RT_DETAIL_PATH = path.join(RT_PATH, 'detail')
RT_REVIEWS_PATH = path.join(RT_PATH, 'reviews')

file_handles = list()


class TestBoxOfficeMojo(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        test_common.setup()

    @classmethod
    def tearDownClass(cls):
        test_common.teardown()

    @patch('requests.get', side_effect=test_common.mocked_requests_get)
    @patch('sys.stdout')
    @patch('sys.stderr')
    def test_run_default(self, _, __, ___):
        abs_path = mkdtemp()
        try:
            imdb = IMDB(abs_path)
            copyfile(path.join(test_common.TEST_DATA_PATH, IMDB.FILENAME), imdb.results_path)
            bom = BoxOfficeMojo(abs_path, imdb.results_path)
            bom.run()
            expected = load_results(path.join(test_common.TEST_DATA_PATH, 'weekend_{0}'.format(BoxOfficeMojo.FILENAME)))
            actual = load_results(bom.results_path)
            self.assertEqual(expected, actual)
        finally:
            rmtree(abs_path)

    @patch('requests.get', side_effect=test_common.mocked_requests_get)
    @patch('sys.stdout')
    @patch('sys.stderr')
    def test_run_daily(self, _, __, ___):
        abs_path = mkdtemp()
        try:
            imdb = IMDB(abs_path)
            copyfile(path.join(test_common.TEST_DATA_PATH, IMDB.FILENAME), imdb.results_path)
            bom = BoxOfficeMojo(abs_path, imdb.results_path)
            bom.run('daily')
            expected = load_results(path.join(test_common.TEST_DATA_PATH, 'daily_{0}'.format(BoxOfficeMojo.FILENAME)))
            actual = load_results(bom.results_path)
            self.assertEqual(expected, actual)
        finally:
            rmtree(abs_path)

    @patch('requests.get', side_effect=test_common.mocked_requests_get)
    @patch('sys.stdout')
    @patch('sys.stderr')
    def test_run_weekly(self, _, __, ___):
        abs_path = mkdtemp()
        try:
            imdb = IMDB(abs_path)
            copyfile(path.join(test_common.TEST_DATA_PATH, IMDB.FILENAME), imdb.results_path)
            bom = BoxOfficeMojo(abs_path, imdb.results_path)
            bom.run('weekly')
            expected = load_results(path.join(test_common.TEST_DATA_PATH, 'weekly_{0}'.format(BoxOfficeMojo.FILENAME)))
            actual = load_results(bom.results_path)
            self.assertEqual(expected, actual)
        finally:
            rmtree(abs_path)

    @patch('requests.get', side_effect=test_common.mocked_requests_get)
    @patch('sys.stdout')
    @patch('sys.stderr')
    def test_run_partial(self, _, __, ___):
        abs_path = mkdtemp()
        try:
            imdb = IMDB(abs_path)
            bom = BoxOfficeMojo(abs_path, imdb.results_path)
            bom_test_data_path = path.join(test_common.TEST_DATA_PATH, 'weekend_{0}'.format(BoxOfficeMojo.FILENAME))

            copyfile(path.join(test_common.TEST_DATA_PATH, IMDB.FILENAME), imdb.results_path)
            save_results(load_results(bom_test_data_path)[0:5], bom.results_path)
            bom.run()
            expected = load_results(path.join(test_common.TEST_DATA_PATH, 'weekend_{0}'.format(BoxOfficeMojo.FILENAME)))
            actual = load_results(bom.results_path)
            self.assertEqual(expected, actual)
        finally:
            rmtree(abs_path)

    @patch('requests.get', side_effect=test_common.mocked_requests_get)
    @patch('sys.stderr')
    def test_get_title_url_daily(self, _, __):
        asserts = 0
        movies = [
            ('Corpse Bride', 2005, 'https://www.boxofficemojo.com/movies/?id=corpsebride.htm&page=daily&view=chart'),
            ('Kate & Leopold', 2001, 'https://www.boxofficemojo.com/movies/?id=kateandleopold.htm&page=daily&view=chart'),
            ('Men in Black II', 2002, 'https://www.boxofficemojo.com/movies/?id=meninblack2.htm&page=daily&view=chart'),
            ('Moonlight Blade', 2009, None),
            ('Na Boca da Noite', 2016, None),
            ('On Parole', 2001, None),
            ('Star Wars: Episode II - Attack of the Clones', 2002,
             'https://www.boxofficemojo.com/movies/?id=starwars2.htm&page=daily&view=chart'),
            ('Star Wars: Episode III - Revenge of the Sith', 2005,
             'https://www.boxofficemojo.com/movies/?id=starwars3.htm&page=daily&view=chart'),
            ('The Emperor\'s New Groove', 2000,
             'https://www.boxofficemojo.com/movies/?id=emperorsnewgroove.htm&page=daily&view=chart'),
            ('The Other Side of the Wind', 2018, None),
            ('Titan A.E.', 2000, 'https://www.boxofficemojo.com/movies/?id=titanae.htm&page=daily&view=chart'),
            ('X-Men', 2000, 'https://www.boxofficemojo.com/movies/?id=xmen.htm&page=daily&view=chart'),
        ]

        for title, year, url in movies:
            self.assertEqual(BoxOfficeMojo.get_title_url(title, year, 'daily'), url)
            asserts += 1
        self.assertEqual(asserts, len(movies))

    @patch('requests.get', side_effect=test_common.mocked_requests_get)
    @patch('sys.stderr')
    def test_get_title_url_weekend(self, _, __):
        asserts = 0
        movies = [
            ('Corpse Bride', 2005, 'https://www.boxofficemojo.com/movies/?id=corpsebride.htm&page=weekend'),
            ('Kate & Leopold', 2001, 'https://www.boxofficemojo.com/movies/?id=kateandleopold.htm&page=weekend'),
            ('Men in Black II', 2002, 'https://www.boxofficemojo.com/movies/?id=meninblack2.htm&page=weekend'),
            ('Moonlight Blade', 2009, None),
            ('Na Boca da Noite', 2016, None),
            ('On Parole', 2001, None),
            ('Star Wars: Episode II - Attack of the Clones', 2002,
             'https://www.boxofficemojo.com/movies/?id=starwars2.htm&page=weekend'),
            ('Star Wars: Episode III - Revenge of the Sith', 2005,
             'https://www.boxofficemojo.com/movies/?id=starwars3.htm&page=weekend'),
            ('The Emperor\'s New Groove', 2000,
             'https://www.boxofficemojo.com/movies/?id=emperorsnewgroove.htm&page=weekend'),
            ('The Other Side of the Wind', 2018, None),
            ('Titan A.E.', 2000, 'https://www.boxofficemojo.com/movies/?id=titanae.htm&page=weekend'),
            ('X-Men', 2000, 'https://www.boxofficemojo.com/movies/?id=xmen.htm&page=weekend'),
        ]

        for title, year, url in movies:
            self.assertEqual(BoxOfficeMojo.get_title_url(title, year, 'weekend'), url)
            asserts += 1
        self.assertEqual(asserts, len(movies))
