import unittest
from os import path
from shutil import rmtree, copyfile
from tempfile import mkdtemp

from mock import patch

import test_common
from common import load_results, save_results
from imdb import IMDB
from rotten_tomatoes import RottenTomatoes

TEST_DATA_PATH = path.join(path.dirname(__file__), 'test_data')
RT_PATH = path.join(TEST_DATA_PATH, 'rotten_tomatoes')
RT_SEARCH_PATH = path.join(RT_PATH, 'search')
RT_DETAIL_PATH = path.join(RT_PATH, 'detail')
RT_REVIEWS_PATH = path.join(RT_PATH, 'reviews')

file_handles = list()


class TestRottenTomatoes(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        test_common.setup()

    @classmethod
    def tearDownClass(cls):
        test_common.teardown()

    @patch('requests.get', side_effect=test_common.mocked_requests_get)
    @patch('sys.stderr')
    def test_get_title_url(self, _, __):
        asserts = 0
        movies = [
            ('Corpse Bride', 2005, 'https://www.rottentomatoes.com/m/corpse_bride'),
            ('Kate & Leopold', 2001, 'https://www.rottentomatoes.com/m/kate_and_leopold'),
            ('Men in Black II', 2002, 'https://www.rottentomatoes.com/m/men_in_black_ii'),
            ('Moonlight Blade', 2009, None),
            ('Na Boca da Noite', 2016, None),
            ('On Parole', 2001, None),
            (
                'Star Wars: Episode II - Attack of the Clones',
                2002,
                'https://www.rottentomatoes.com/m/star_wars_episode_ii_attack_of_the_clones'
            ),
            (

                'Star Wars: Episode III - Revenge of the Sith',
                2005,
                'https://www.rottentomatoes.com/m/star_wars_episode_iii_revenge_of_the_sith'
            ),
            ('The Emperor\'s New Groove', 2000, 'https://www.rottentomatoes.com/m/emperors_new_groove'),
            ('The Other Side of the Wind', 2018, 'https://www.rottentomatoes.com/m/the_other_side_of_the_wind'),
            ('Titan A.E.', 2000, 'https://www.rottentomatoes.com/m/titan_ae'),
            ('X-Men', 2000, 'https://www.rottentomatoes.com/m/xmen'),
        ]

        for movie_tuple in movies:
            self.assertEqual(RottenTomatoes.get_title_url(movie_tuple[0], movie_tuple[1]), movie_tuple[2])
            asserts += 1
        self.assertEqual(asserts, len(movies))

    @patch('requests.get', side_effect=test_common.mocked_requests_get)
    def test_get_movie_id(self, _):
        asserts = 0
        movies = {
            'https://www.rottentomatoes.com/m/corpse_bride': '7591',
            'https://www.rottentomatoes.com/m/kate_and_leopold': '10350',
            'https://www.rottentomatoes.com/m/men_in_black_ii': '10789',
            'https://www.rottentomatoes.com/m/star_wars_episode_ii_attack_of_the_clones': '10009',
            'https://www.rottentomatoes.com/m/star_wars_episode_iii_revenge_of_the_sith': '9',
            'https://www.rottentomatoes.com/m/emperors_new_groove': '9457',
            'https://www.rottentomatoes.com/m/the_other_side_of_the_wind': '771497514',
            'https://www.rottentomatoes.com/m/titan_ae': '10688',
            'https://www.rottentomatoes.com/m/xmen': '11206',
        }

        for title, url in movies.iteritems():
            self.assertEqual(RottenTomatoes.get_movie_id(title), url)
            asserts += 1
        self.assertEqual(asserts, len(movies))

    @patch('requests.get', side_effect=test_common.mocked_requests_get)
    @patch('sys.stdout')
    @patch('sys.stderr')
    def test_run(self, _, __, ___):
        abs_path = mkdtemp()
        try:
            imdb = IMDB(abs_path)
            copyfile(path.join(test_common.TEST_DATA_PATH, IMDB.FILENAME), imdb.results_path)
            rt = RottenTomatoes(abs_path, imdb.results_path)
            rt.run()
            expected = load_results(path.join(test_common.TEST_DATA_PATH, RottenTomatoes.FILENAME))
            actual = load_results(rt.results_path)
            self.assertEqual(expected, actual)
        finally:
            rmtree(abs_path)

    @patch('requests.get', side_effect=test_common.mocked_requests_get)
    @patch('sys.stdout')
    @patch('sys.stderr')
    def test_run_partial(self, _, __, ___):
        abs_path = mkdtemp()
        try:
            imdb = IMDB(abs_path)
            rt = RottenTomatoes(abs_path, imdb.results_path)
            rt_test_data_path = path.join(test_common.TEST_DATA_PATH, RottenTomatoes.FILENAME)

            copyfile(path.join(test_common.TEST_DATA_PATH, IMDB.FILENAME), imdb.results_path)
            save_results(load_results(rt_test_data_path)[0:5], rt.results_path)

            rt.run()
            expected = load_results(path.join(test_common.TEST_DATA_PATH, RottenTomatoes.FILENAME))
            actual = load_results(rt.results_path)
            self.assertEqual(expected, actual)
        finally:
            rmtree(abs_path)
