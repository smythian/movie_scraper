import unittest
from os import path
from shutil import rmtree
from tempfile import mkdtemp

from mock import patch

import test_common
from common import load_results
from imdb import IMDB


class TestIMDB(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        test_common.setup()

    @classmethod
    def tearDownClass(cls):
        test_common.teardown()

    @patch('requests.get', side_effect=test_common.mocked_requests_get)
    def test_imdb(self, _):
        abs_path = mkdtemp()
        try:
            imdb = IMDB(abs_path)
            imdb.run(2000, 2018, 0)
            expected = load_results(path.join(test_common.TEST_DATA_PATH, IMDB.FILENAME))
            actual = load_results(imdb.results_path)
            self.assertEqual(expected, actual)
        finally:
            rmtree(abs_path)
