import json
import unittest
from os import path
from shutil import rmtree
from tempfile import mkdtemp

from mock import patch

import do_work
import test_common


class TestDoWork(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        test_common.setup()

    @classmethod
    def tearDownClass(cls):
        test_common.teardown()

    @patch('requests.get', side_effect=test_common.mocked_requests_get)
    @patch('sys.stdout')
    @patch('sys.stderr')
    def test_main(self, _, __, ___):
        abs_path = mkdtemp()
        try:
            with patch('sys.argv', [
                'Hi mom!',
                '--path', abs_path,
                '--oldest', '2017',
                '--newest', '2017',
                '--ratings', '400000',
                '--box', 'daily'
            ]):
                do_work.main()
                actual_path = path.join(abs_path, 'results.json')
                expected_path = path.join(test_common.TEST_DATA_PATH, 'results.json')
                with open(actual_path, 'r') as actual_file, open(expected_path, 'r') as expected_file:
                    actual = json.load(actual_file)
                    expected = json.load(expected_file)
                    self.assertEqual(actual, expected)
        finally:
            rmtree(abs_path)
