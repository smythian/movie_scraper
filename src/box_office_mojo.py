import json
import re
from os import path
from requests import exceptions

from bs4 import BeautifulSoup

import common


class BoxOfficeMojo:
    FILENAME = 'box_office_mojo.results'
    BOM_URL = 'https://www.boxofficemojo.com'
    QUERY_URL = BOM_URL + '/search/'
    TIME_UNITS = {
        'daily': {
            'searchQueryParams': '&page=daily&view=chart',
            'selector': 'table.chart-wide tr'
        },
        'weekend': {
            'searchQueryParams': '&page=weekend',
            'selector': '#body > table > tbody > tr > td > center table tr'
        },
        'weekly': {
            'searchQueryParams': '&page=weekly',
            'selector': '#body > table > tbody > tr > td > center table tr'
        }
    }

    @staticmethod
    def get_err_handler(err):
        result = True
        if isinstance(err, exceptions.HTTPError):
            if err.response.status_code == 403:
                result = False
        else:
            raise err
        return result

    @staticmethod
    def parse_year(search_result_row):
        result = None
        for el in search_result_row.findAll('td'):
            match = re.search(r'(?<=\d/)\d{4}$', el.text.strip())
            if match:
                result = int(match.group(0))
            elif el.text == 'multiple':
                result = el.text
        return result

    @staticmethod
    def map_search_results(search_result_row, time_unit):
        result = {'year': None, 'title': None}
        year = BoxOfficeMojo.parse_year(search_result_row)
        title_el = search_result_row.find('b')
        if year:
            result['year'] = year
        if title_el:
            result['title'] = title_el.text
            href_el = title_el.find('a')
            if href_el:
                result['href'] = u'{0}{1}'\
                    .format(href_el.attrs['href'], BoxOfficeMojo.TIME_UNITS[time_unit]['searchQueryParams'])
                if not result['href'].startswith('https://'):
                    result['href'] = u'{0}{1}'.format(BoxOfficeMojo.BOM_URL, result['href'])
        return result

    @staticmethod
    def get_title_url(title, year, time_unit):
        data = {'q': title.encode('utf-8')}
        response = common.get_request(BoxOfficeMojo.QUERY_URL, BoxOfficeMojo.get_err_handler, params=data)
        result = None
        if response:
            search_result_selector = '#body > table:nth-of-type(2) td > table:nth-of-type(2) tr'
            search_result_rows = BeautifulSoup(response.text, "html5lib").select(search_result_selector)
            mapped_results = map(lambda movie: BoxOfficeMojo.map_search_results(movie, time_unit), search_result_rows)
            year_matches = filter(lambda movie: movie['year'] in [year, 'multiple'], mapped_results)
            exact_name_match = filter(lambda movie: common.exactish_match(movie['title'], title), year_matches)
            if len(exact_name_match) == 1:
                result = exact_name_match[0]['href']
            else:
                name_ends_with_match = filter(lambda movie: movie['title'].endswith(title), year_matches)
                if len(name_ends_with_match) == 1:
                    result = name_ends_with_match[0]['href']
                else:
                    name_starts_with_match = \
                        filter(lambda movie: re.match(u'{0} \({1}\)'.format(title, year), movie['title']), year_matches)
                    if len(name_starts_with_match) == 1:
                        result = name_starts_with_match[0]['href']
            if not result:
                if year_matches:
                    matches = '"{0}"'.format('", "'.join(map(lambda movie: movie['title'], year_matches)))
                    msg = u'Search for "{0}" ({1}) could not be identified among: {2}'.format(title, year, matches)
                    common.logger.warn(msg)
                else:
                    common.logger.debug(u'No matches for "{0}" ({1})'.format(title, year))
        return result

    @staticmethod
    def get_text_for_cell(cells, index):
        return cells[index].text if 0 <= index < len(cells) else 'PARSE ERROR'

    @staticmethod
    def map_box_office(cells, time_unit):
        if time_unit == 'daily':
            result = {
                'day': BoxOfficeMojo.get_text_for_cell(cells, 0),
                'date': BoxOfficeMojo.get_text_for_cell(cells, 1),
                'rank': BoxOfficeMojo.get_text_for_cell(cells, 2),
                'gross': BoxOfficeMojo.get_text_for_cell(cells, 3),
                'percent_change_day': BoxOfficeMojo.get_text_for_cell(cells, 4),
                'percent_change_week': BoxOfficeMojo.get_text_for_cell(cells, 5),
                'num_theaters': BoxOfficeMojo.get_text_for_cell(cells, 6),
                'avg_gross_per_theater': BoxOfficeMojo.get_text_for_cell(cells, 7),
                'gross_to_date': BoxOfficeMojo.get_text_for_cell(cells, 8),
                'day_num': BoxOfficeMojo.get_text_for_cell(cells, 9)
            }
        else:  # The site uses the same structure for weekly and weekend box office data
            result = {
                'date': BoxOfficeMojo.get_text_for_cell(cells, 0),
                'rank': BoxOfficeMojo.get_text_for_cell(cells, 1),
                'gross': BoxOfficeMojo.get_text_for_cell(cells, 2),
                'percent_change': BoxOfficeMojo.get_text_for_cell(cells, 3),
                'num_theaters': BoxOfficeMojo.get_text_for_cell(cells, 4),
                'theaters_change': BoxOfficeMojo.get_text_for_cell(cells, 5),
                'avg_gross_per_theater': BoxOfficeMojo.get_text_for_cell(cells, 6),
                'gross_to_date': BoxOfficeMojo.get_text_for_cell(cells, 7),
                'week_num': BoxOfficeMojo.get_text_for_cell(cells, 8)
            }
        return result

    @staticmethod
    def get_box_office(title_url, time_unit):
        result = {u'url': title_url}
        response = common.get_request(title_url, BoxOfficeMojo.get_err_handler)
        soup = BeautifulSoup(response.text, 'html5lib')
        # Movie box office metadata
        meta = soup.select('table table table table td')
        for cell in meta:
            parts = cell.text.split(':')
            if len(parts) == 2:
                key = parts[0].strip().lower().replace(' ', '_')
                result[key] = parts[1].strip()

        # Daily box office data
        box_office = soup.select('table.chart-wide tr')
        # Header and divider rows use the background color #dcdcdc
        box_office = filter(lambda row: 'bgcolor' in row.attrs and row.attrs['bgcolor'] != '#dcdcdc', box_office)
        result[time_unit] = map(lambda row: BoxOfficeMojo
                                .map_box_office(row.findChildren('td', recursive=False), time_unit), box_office)
        return result

    def __init__(self, dir_path, titles_path):
        self.titles_path = titles_path
        self.results_path = path.join(dir_path, BoxOfficeMojo.FILENAME)

    def run(self, time_unit='weekend'):
        common.logger.info('Beginning to scrape box office data')
        with open(self.results_path, 'a+') as results_file, open(self.titles_path, 'r') as titles_file:
            resume_count = 0
            success_count = 0
            total_count = 0
            for _ in results_file:
                resume_count += 1
                titles_file.next()
            common.logger.info('Loaded {0} movies from previous run'.format(resume_count))
            for row in titles_file:
                movie = json.loads(row.encode('utf-8'))
                title = movie['title']
                year = movie['year']
                url = BoxOfficeMojo.get_title_url(title, year, time_unit)
                movie['box_office_mojo'] = {}
                if url:
                    movie['box_office_mojo'] = BoxOfficeMojo.get_box_office(url, time_unit)
                    common.logger.debug(u'Found "{0}" ({1})'.format(title, year))
                    success_count += 1
                results_file.write(u'{0}\n'.format(json.dumps(movie)).encode('utf-8'))
                total_count += 1
        common.logger.info('Completed scraping for {0} of {1} movies'.format(success_count, total_count))
