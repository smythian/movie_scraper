import collections
import json
import logging
import requests
from csv import DictWriter, DictReader
from gzip import open as gzipopen
import math
from os import path
from shutil import copyfileobj
from sys import stdout
from time import sleep, time, gmtime

MAX_REQUESTS_PER_SEC = 0
MAX_RETRIES = 7
SLEEP_DELAY = 2  # 1 2 4 8 16 32 64
LOG_FORMAT = '%(asctime)s %(levelname)s [%(module)s] %(message)s'
LOG_TIME_FORMAT = '%Y-%m-%dT%H:%M:%S%z'

EQUIVALENTS = {
    '&': 'and',
    'II': '2',
    'III': '3',
    'IV': '4',
    'V': '5',
    'VI': '6',
    'VII': '7',
    'VIII': '8',
    'IX': '9',
}

logging.Formatter.converter = gmtime
log_format = logging.Formatter(LOG_FORMAT, LOG_TIME_FORMAT)
logger = logging.getLogger()

console_logger = logging.StreamHandler(stdout)
console_logger.setFormatter(log_format)
console_logger.setLevel(logging.INFO)

logger.addHandler(console_logger)


def init_file_logger(file_path):
    file_logger = logging.FileHandler(path.join(file_path, '{0}_{1}'.format(get_time_in_ms(), 'movie.log')), 'w')
    file_logger.setFormatter(log_format)
    file_logger.setLevel(logging.DEBUG)
    logger.addHandler(file_logger)


def exactish_match(a, b):
    for key, value in EQUIVALENTS.iteritems():
        a = a.replace(key, value)
        b = b.replace(key, value)
    return a.lower().strip() == b.lower().strip()


def flatten_obj(d, parent_key='', sep='_'):
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, collections.MutableMapping):
            items.extend(flatten_obj(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


def update_set(acc, o):
    acc.update(o.keys())
    return acc


def flatten_result(result):
    flat = map(flatten_obj, result)
    keys = reduce(update_set, flat, set())
    return list(keys), flat


def save_csv_like_file(result, file_path, delimiter=','):
    fieldnames, flat_result = flatten_result(result)
    with open(file_path, 'w') as result_file:
        writer = DictWriter(result_file, fieldnames=fieldnames, delimiter=delimiter)
        writer.writeheader()
        for row in flat_result:
            writer.writerow({k: u'{0}'.format(v).encode('utf-8') for k, v in row.items()})


def load_csv_like_file(file_path, delimiter=','):
    result = []
    with open(file_path, 'r') as result_file:
        reader = DictReader(result_file, delimiter=delimiter)
        for row in reader:
            result.append({unicode(key, "utf-8"): unicode(value, "utf-8") for key, value in row.iteritems()})
    return result


def save_results(result, file_path):
    if file_path.endswith('json'):
        with open(file_path, 'w') as result_file:
            json.dump(result, result_file)
    elif file_path.endswith('csv'):
        save_csv_like_file(result, file_path)
    elif file_path.endswith('tsv'):
        save_csv_like_file(result, file_path, '\t')
    elif file_path.endswith('results'):
        with open(file_path, 'w') as result_file:
            for movie in result:
                result_file.write(u'{0}\n'.format(json.dumps(movie)).encode('utf-8'))
    else:
        raise RuntimeError('Invalid output file format: "{0}"'.format(file_path))


def load_results(file_path):
    if file_path.endswith('json'):
        with open(file_path, 'r') as result_file:
            result = json.load(result_file)
    elif file_path.endswith('csv'):
        result = load_csv_like_file(file_path)
    elif file_path.endswith('tsv'):
        result = load_csv_like_file(file_path, '\t')
    elif file_path.endswith('results'):
        with open(file_path, 'r') as result_file:
            # result = map(lambda line: json.loads(line.encode('utf-8')), result_file.readlines())
            result = []
            count = 0
            for line in result_file:
                count += 1
                try:
                    result.append(json.loads(line.encode('utf-8')))
                except ValueError:
                    logger.error('Error on line {0}'.format(count))
    else:
        raise RuntimeError('Invalid input file format: "{0}"'.format(file_path))
    return result


def download_binary_file_in_chunks(url, file_path):
    response = requests.get(url, stream=True)
    with open(file_path, 'wb') as f:
        for chunk in response.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
                f.flush()


def extract_gzip_file(archive_path, output_path):
    with gzipopen(archive_path, 'rb') as f_in:
        with open(output_path, 'wb') as f_out:
            copyfileobj(f_in, f_out)


def get_time_in_ms():
    """
    :return: The current time in milliseconds
    """
    return int(round(time() * 1000))


def limit_request_rate(start_time):
    """
    Delays the next request to avoid running afoul of API rate limits
    :param start_time: The time in milliseconds that the request was started
    """
    delay = MAX_REQUESTS_PER_SEC - (get_time_in_ms() - start_time)
    if delay > 0:
        sleep(delay)


def get_request(url, err_handler=None, **kwargs):
    """
    Wraps a get request in basic retry and rate limiting logic

    :param url: The URL to get
    :param err_handler: Request specific logic regarding which HTTP errors are recoverable
    :param kwargs: Other arguments (e.g., params)
    :return: The response object
    """
    retries = 0
    result = None
    start_time = 0
    while result is None and retries < MAX_RETRIES:
        try:
            start_time = get_time_in_ms()
            result = requests.get(url, **kwargs)
            result.raise_for_status()
        except requests.exceptions.HTTPError as err:
            if err_handler(err):
                logger.warn('GET attempt {0} for "{1}" ({2}) failed.'.format(retries + 1, url, json.dumps(kwargs)))
                retries += 1
                sleep(math.pow(SLEEP_DELAY, retries))
            else:
                break
        finally:
            limit_request_rate(start_time)
    return result
