# Movie data scraper

[![pipeline status](https://gitlab.com/smythian/movie_scraper/badges/master/pipeline.svg)](https://gitlab.com/smythian/movies/commits/master)
[![coverage report](https://gitlab.com/smythian/movie_scraper/badges/master/coverage.svg)](https://gitlab.com/smythian/movies/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/593281cf16f74bdd866697ce1b5c5aa4)](https://www.codacy.com/app/ecsmyth/movie_scraper?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=smythian/movie_scraper&amp;utm_campaign=Badge_Grade)
[![License](https://img.shields.io/badge/license-MIT-green.svg)](https://gitlab.com/smythian/gunrock/blob/master/LICENSE)

Scripts to pull movie title and ratings data from IMDB, ticket sales data from
Box Office Mojo, and review data from Rotten Tomatoes. 

## Getting Started

The IMDB class removes any movies without IMDB ratings to reduce the number of queries. Using a DigitalOcean VM (s-1vcpu-2gb), the single-threaded approach benchmarks at approximately 2 movies/s for Box Office Mojo and 1 movie/s for Rotten Tomatoes.

### Prerequisites

* [Python 2.7](https://www.python.org/download/releases/2.7/)
* [pip](https://pip.pypa.io/en/stable/installing/)
* [pipenv](https://pypi.org/project/pipenv/)
* Dependencies `pipenv install`. Note: Other versions of required packages may work but have not been tested.

### Commands

#### Run script
`pipenv run python do_work.py [-h] [-v] [-o OLDEST] [-n NEWEST] [-r RATINGS] [-f FORMAT] [-p PATH]`

optional arguments:
* `-h`, `--help` show this help message and exit
* `-v`, `--version` show program's version number and exit
* `-o OLDEST`, `--oldest OLDEST` Earliest release year to include. Default is "2017".
* `-n NEWEST`, `--newest NEWEST` Last release year to include. Default is "2018".
* `-r RATINGS`, `--ratings RATINGS` Minimum number of ratings on IMDB (99.5 percent have fewer than 160). Default is "160".
* `-b BOX`, `--box BOX` Box office data to pull. Options are "daily", "weekend", or "weekly". Default is "weekend".
* `-f FORMAT`, `--format FORMAT` File format for the results. Options are "csv", "tsv", and "json" (default).
* `-p PATH`, `--path PATH` Directory where results should be saved. Default is
                        "/home/ecsmyth/Development/rt/src/interim_results".

#### Run unit tests

```bash
pipenv run python -m unittest discover -s src/
```

#### Collect code coverage
```bash
pipenv run coverage run --source=src -m unittest discover -s src/
pipenv run coverage report
```

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/mastiff/lurcher/tags). 

## Authors

* [Eric Smyth](https://gitlab.com/smythian)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
